# AMP Story

This page was build with AMP and the AMP Story Module.

## Setup

To Start a new AMP page your should use the AMP boilplate generator this can be fund here:
https://amp.dev/boilerplate/

## Development 

If you want to Debug this Project you have to open the index.html in a 
web browser or start a local server which hosts this index.html as a webpage.

AMP is pretty simple to debug you just have to add the following to your address: 
```
#development=1
```

For example:
```
https://brandneo.de/#development=1
```

Open the Developer Console in Chrome (or your preferred browser), and verify there are no AMP errors. You might need to refresh your browser to see validation messages. If your page is free of errors, you should see the message:

```
 AMP validation successful.
```

## Styling and Fonts

In AMP all Styling and Fonts will be added inline into your HTML document.

Everything Information can be fund under: 
https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/custom_fonts/?format=stories

### Fonts

You can embed custom fonts into your AMP page in two ways:

 * Through a <link> tag: for allow-listed font providers only.
 * By using the @font-face CSS rule: there are no restrictions, all fonts are allowed.

#### Using <link>

```
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine">
```

The following origins are allowlisted and allowed for font serving via link tags:

 * Typography.com: https://cloud.typography.com
 * Fonts.com: https://fast.fonts.net
 * Google Fonts: https://fonts.googleapis.com
 * Typekit: https://use.typekit.net
 * Font Awesome: https://maxcdn.bootstrapcdn.com, https://use.fontawesome.com

#### Using @font-face

Alternatively, you can use @font-face within your AMP stylesheet:

```
<style amp-custom>
  @font-face {
    font-family: "Bitstream Vera Serif Bold";
    src: url("https://somedomain.org/VeraSeBd.ttf");
  }

  body {
    font-family: "Bitstream Vera Serif Bold", serif;
  }
</style>
```

### CSS

Like all web pages, AMP pages are styled with CSS, but you can’t reference external stylesheets (with the exception of custom fonts). Also certain styles are disallowed due to performance implications.

Everything Information can be fund under: 
https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/style_pages/?format=stories


## Image Compression

In AMP you should use compressed Images to have better performance which is really important.

### Image Tool

For your image compression i would recommend a Tool where you can choose the file output and compression rate.
The Tool what i used is Squoosh here it is pretty easy and you can get webp format.

https://squoosh.app/

### Import Images 

In AMP there are 2 ways of performing a better image loading.

1. Using webp images where is it possible
2. Using async loading for the first images 

#### <b>Using webp</b>

AMP has the feature to provide webp images which can be only displayed in Google Chrome.
To have a fallback when users visit your side from other browsers you have to implement it like this.

```
    <amp-img width="80" height="80" layout="fixed" src="assets/logo/brandneo-simple-white.webp">
        <amp-img fallback src="assets/logo/brandneo-simple-white.png" width="80" height="80" layout="fixed"
            alt="brandneo_simple">
        </amp-img>
    </amp-img>
```

#### <b>Using async</b>

It is possible to preload the image which will be displayed for the first view.
This should be only used for images of the first storys.

```
  <link rel="preload" href="assets/images/cover.jpg" as="image">
  <link rel="preload" href="assets/images/page1.jpg" as="image">
  <link rel="preload" href="assets/images/page2.jpg" as="image">
```
